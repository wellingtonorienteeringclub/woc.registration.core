﻿namespace WOC.Registration.Core {
  public class Grade {
    public string Id { get; set; }
    public string ShortName { get; set; }
    public string Name { get; set; }
    public string CourseId { get; set; }
  }
}