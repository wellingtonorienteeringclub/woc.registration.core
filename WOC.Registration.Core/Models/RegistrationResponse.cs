﻿namespace WOC.Registration.Core {
  public class RegistrationResponse {
    public string Message { get; set; }
    public bool Registered { get; set; }
  }
}