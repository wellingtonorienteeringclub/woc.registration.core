﻿using System;

namespace WOC.Registration.Core {
  public class Punch {
    public string Id { get; set; }
    public DateTime? Time { get; set; }
  }
}