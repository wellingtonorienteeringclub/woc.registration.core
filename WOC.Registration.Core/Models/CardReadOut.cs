﻿using System.Collections.Generic;

namespace WOC.Registration.Core {
  public class CardReadOut {
    public string Card { get; set; }
    public List<Punch> Punches { get; set; }
  }
}