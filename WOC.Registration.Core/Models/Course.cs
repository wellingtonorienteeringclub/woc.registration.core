﻿using System.Collections.Generic;

namespace WOC.Registration.Core {
  public class Course {
    public string Id { get; set; }
    public string Length { get; set; }
    public string Climb { get; set; }
    public string Description { get; set; }
    public List<int> ControlNumbers { get; set; }
  }
}