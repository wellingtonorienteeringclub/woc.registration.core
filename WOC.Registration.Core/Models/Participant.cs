﻿using System;

namespace WOC.Registration.Core {
  public class Participant {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Chip { get; set; }
    public string Club { get; set; }
    public string Grade { get; set; }
    public string GradeId { get; set; }
    public string Id { get; set; }
    public bool? Male { get; set; }
    public string ClubId { get; set; }
    public DateTime? Start { get; set; }
    public DateTime? Finish { get; set; }
    public int? Time { get; set; }
    public string Status { get; set; }
  }
}