﻿namespace WOC.Registration.Core {
  public class Club {
    public string Id { get; set; }
    public string Name { get; set; }
    public string ShortName { get; set; }
  }
}