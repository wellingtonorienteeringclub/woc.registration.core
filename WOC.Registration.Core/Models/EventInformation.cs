﻿using System;

namespace WOC.Registration.Core {
  public class EventInformation {
    public string Name { get; set; }
    public DateTime? ZeroTime { get; set; }
  }
}