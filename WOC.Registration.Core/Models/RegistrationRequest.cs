﻿namespace WOC.Registration.Core {
  public class RegistrationRequest {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Club { get; set; }
    public string Grade { get; set; }
    public string Chip { get; set; }
  }
}