﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WOC.Registration.Core {
  public interface IDataProvider {
    Task<List<Participant>> GetParticipants();
    Task<EventInformation> GetEvent();
    Task<List<Grade>> GetGrades();
    Task<List<Course>> GetCourses();
    Task<List<Club>> GetClubs();
    Task<RegistrationResponse> Register(RegistrationRequest request);
    Task<List<CardReadOut>> GetPunches();
  }
}